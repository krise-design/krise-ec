var headerTemplate = `
<nav id="mobile-nav" class="small collapse">
        <div class="container">
            <div class="row my-2">
                <div class="col text-right">
                    <button id="close-mobile-nav" class="btn btn-lg btn-link text-dark">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="row my-2">
                <div class="col">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" placeholder="Search by keyword or product number"
                            aria-label="Search by keyword or product number">
                        <select class="form-control" id="siteSearchSelect">

                            <option value="" selected="">All Categories</option>

                            <option value="EASYCommerce">EASYCommerce</option>

                            <option value="Fasteners">Fasteners</option>

                            <option value="Fleet Maintenance">Fleet Maintenance</option>

                            <option value="Hand Tools">Hand Tools</option>

                            <option value="Hardware">Hardware</option>

                            <option value="Holemaking">Holemaking</option>

                            <option value="HVAC">HVAC</option>

                            <option value="Lighting &amp; Electrical">Lighting &amp; Electrical</option>

                            <option value="Material Handling &amp; Storage">Material Handling &amp; Storage</option>

                            <option value="Saw Blades">Saw Blades</option>

                        </select>

                        <div class="input-group-append">
                            <button class="btn btn-primary rounded-right" type="button">
                                <i class="fas fa-search"></i><span class="sr-only">Search</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="">
                                Lighting & Electrical <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="">
                                EASYCommerce <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="">
                                Fasteners <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="">
                                Abrasives <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="">
                                Saw Blades <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="">
                                Hand Tools <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="">
                                Fleet Maintenance <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row mt-3 border-top">
                <div class="col-auto">
                    <a href="" class="btn btn-link"><i class="fas fa-user"></i> Sign In/Register</a>
                </div>

                <div class="col-6">

                </div>
            </div>
    </nav>

    <header class="sticky-top">
        <div class="container-fluid bg-dark text-white">
            <div class="row no-gutters justify-content-end">
                <div class="col-auto">
                    <button class="btn btn-sm dropdown-toggle text-white text-uppercase" type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Language</button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item active" href="#">English</a>
                        <a class="dropdown-item" href="#">Français</a>
                        <a class="dropdown-item" href="#">Deutsch</a>
                        <a class="dropdown-item" href="#">Español</a>
                        <a class="dropdown-item" href="#">Italiano</a>
                        <a class="dropdown-item" href="#">Nederlands</a>
                        <a class="dropdown-item" href="#">Português</a>
                        <a class="dropdown-item" href="#">Русский</a>
                        <a class="dropdown-item" href="#">简体中文</a>
                        <a class="dropdown-item" href="#">繁體中文</a>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="btn btn-sm btn-link text-light text-decoration-none">
                        <span class="text-uppercase">Notifications</span>
                        <span class="badge bg-danger">8</span>
                    </div>
                </div>
                <div class="col-auto mr-1 mr-lg-3">
                    <button class="btn btn-sm btn-third text-light text-uppercase">
                        <span class="fas fa-comments"></span> Chat
                    </button>
                </div>
            </div>
        </div>

        <nav class="container-fluid">
            <div class="row align-items-center py-2 py-md-0 bg-light">
                <div class="col-4 d-lg-none">
                    <button id="mobile-nav-toggle" class="btn btn-outline-dark" type="button"
                        aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                        <span class="sr-only">Toggle Navigation</span>
                    </button>
                </div>
                <div class="col-4 col-md-2">
                    <a href="/" class="d-inline-block">
                        <span class="sr-only">Home</span>
                        <img src="images/company-logo.png" alt="K-Rise Systems" class="img-fluid py-2 px-3" />
                    </a>
                </div>

                <div class="col-7 my-auto d-none d-lg-block">
                    <div class="">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search by keyword or product number"
                                aria-label="Search by keyword or product number">
                            <select class="form-select" id="siteSearchSelect" style="min-width: 6rem; max-width: 12rem;">

                                <option value="" selected="">All Categories</option>

                                <option value="EASYCommerce">EASYCommerce</option>

                                <option value="Fasteners">Fasteners</option>

                                <option value="Fleet Maintenance">Fleet Maintenance</option>

                                <option value="Hand Tools">Hand Tools</option>

                                <option value="Hardware">Hardware</option>

                                <option value="Holemaking">Holemaking</option>

                                <option value="HVAC">HVAC</option>

                                <option value="Lighting &amp; Electrical">Lighting &amp; Electrical</option>

                                <option value="Material Handling &amp; Storage">Material Handling &amp; Storage</option>

                                <option value="Saw Blades">Saw Blades</option>

                            </select>

                            <div class="btn-group">
                                <button class="btn btn-primary rounded-right" type="button">
                                    <i class="fas fa-search"></i><span class="sr-only">Search</span>
                                </button>

                                <button type="button" class="btn btn-primary dropdown-toggle ml-3" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    Bulk Pad
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" style="width: 18rem;">
                                    <section id="headerBulkDropdown" class="px-3">
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="dropdown-bulk-rows" role="tabpanel"
                                                aria-labelledby="dropdown-bulk-rows-tab">
                                                <!-- tab for rows -->
                                                <div class="form-row mb-3">
                                                    <div class="col-8">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-1" placeholder="Part #">
                                                        <label for="dropdown-bulkpad-line-1" class="sr-only">Part #</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-1-qty" placeholder="Qty">
                                                        <label for="dropdown-bulkpad-line-1-qty" class="sr-only">Qty</label>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-3">
                                                    <div class="col-8">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-2" placeholder="Part #">
                                                        <label for="dropdown-bulkpad-line-2" class="sr-only">Part #</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-2-qty" placeholder="Qty">
                                                        <label for="dropdown-bulkpad-line-2-qty" class="sr-only">Qty</label>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-3">
                                                    <div class="col-8">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-3" placeholder="Part #">
                                                        <label for="dropdown-bulkpad-line-3" class="sr-only">Part #</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-3-qty" placeholder="Qty">
                                                        <label for="dropdown-bulkpad-line-3-qty" class="sr-only">Qty</label>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-3">
                                                    <div class="col-8">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-4" placeholder="Part #">
                                                        <label for="dropdown-bulkpad-line-4" class="sr-only">Part #</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-4-qty" placeholder="Qty">
                                                        <label for="dropdown-bulkpad-line-4-qty" class="sr-only">Qty</label>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-3">
                                                    <div class="col-8">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-5" placeholder="Part #">
                                                        <label for="dropdown-bulkpad-line-5" class="sr-only">Part #</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-5-qty" placeholder="Qty">
                                                        <label for="dropdown-bulkpad-line-5-qty" class="sr-only">Qty</label>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-3">
                                                    <div class="col-8">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-6" placeholder="Part #">
                                                        <label for="dropdown-bulkpad-line-6" class="sr-only">Part #</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="number" class="form-control"
                                                            id="dropdown-bulkpad-line-6-qty" placeholder="Qty">
                                                        <label for="dropdown-bulkpad-line-6-qty" class="sr-only">Qty</label>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="col text-center">
                                                        <button class="btn btn-block btn-primary mb-2">Add to Cart</button>
                                                        <button class="btn btn-link btn-sm">Large Order Pad</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-4 col-md-3">
                    <div class="row no-gutters">
                        <div class="d-none d-lg-block col-md-6 text-center border-right dropdown">
                            <div class="text-decoration-none" id="dropdownAccountButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <div class="small">Sign In / Register</div>
                                <div class="text-strong mb-0 text-uppercase">My Account <i
                                        class="far fa-user d-none d-xl-inline-block"></i></div>
                            </div>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownAccountButton"
                                style="width: 16rem;">
                                <form class="px-4 py-3 text-primary">
                                    <div class="form-floating mb-2">
                                        <input type="email" class="form-control" id="dropdownFormEmail1"
                                            placeholder="Email address">
                                        <label for="dropdownFormEmail1">Email address</label>
                                    </div>
                                    <div class="form-floating mb-2">
                                        <input type="password" class="form-control" id="dropdownFormPassword1"
                                            placeholder="Password">
                                        <label for="dropdownFormPassword1">Password</label>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="dropdownCheck">
                                            <label class="form-check-label" for="dropdownCheck">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Sign in</button>
                                </form>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Register</a>
                                <a class="dropdown-item" href="#">Track Your Order</a>
                                <a class="dropdown-item" href="#">Forgot password?</a>
                            </div>
                        </div>
                        <div class="col col-md-6 text-center">
                            <a href="" class="btn btn-outline-dark float-right float-md-none">
                                <i class="fas fa-shopping-cart"></i>
                                <span class="d-none d-lg-inline-block">Cart</span>
                                <span class="badge bg-info text-light">3</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <nav class="d-none d-lg-flex row justify-content-center bg-white">
                <div class="navbar navbar-expand-lg col-auto">
                    <ul class="navbar-nav level-0">
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
                                href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Lighting%20%26%20Electrical%22)">Lighting
                                &amp; Electrical</a>
                            <div class="dropdown-menu">
                                <ul class="level-1">
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Lighting%20%26%20Electrical%22)AND(CategoryCode2:%22Lighting%22)">Lighting</a>
                                    </li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Lighting%20%26%20Electrical%22)AND(CategoryCode2:%22Contactors%20%26%20Starters%22)">Contactors
                                            &amp; Starters</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Lighting%20%26%20Electrical%22)AND(CategoryCode2:%22Electrical%20Sensor%22)">Electrical
                                            Sensor</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Lighting%20%26%20Electrical%22)AND(CategoryCode2:%22Conduits,%20Ducts,%20Routing%22)">Conduits,
                                            Ducts, Routing</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Lighting%20%26%20Electrical%22)AND(CategoryCode2:%22Installation%20Tools%22)">Installation
                                            Tools</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Lighting%20%26%20Electrical%22)AND(CategoryCode2:%22Outlet%20Receptacles%22)">Outlet
                                            Receptacles</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
                                href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22EASYCommerce%22)">EASYCommerce</a>
                            <div class="dropdown-menu">
                                <ul class="level-1">
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22EASYCommerce%22)AND(CategoryCode2:%22Clothing%22)">Clothing</a>
                                    </li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22EASYCommerce%22)AND(CategoryCode2:%22Cartridge%20Filters%22)">Cartridge
                                            Filters</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22EASYCommerce%22)AND(CategoryCode2:%22Packing%20%26%20Shipping%22)">Packing
                                            &amp; Shipping</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22EASYCommerce%22)AND(CategoryCode2:%22Audio%20Technology%22)">Audio
                                            Technology</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22EASYCommerce%22)AND(CategoryCode2:%22Material%20Handling%22)">Material
                                            Handling</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
                                href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fasteners%22)">Fasteners</a>
                            <div class="dropdown-menu">
                                <ul class="level-1">
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fasteners%22)AND(CategoryCode2:%22Bolt,%20Screws,%20Cap%20Screws%22)">Bolt,
                                            Screws, Cap Screws</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fasteners%22)AND(CategoryCode2:%22Pins,%20Clips,%20Retaining%20Rings%22)">Pins,
                                            Clips, Retaining Rings</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fasteners%22)AND(CategoryCode2:%22Anchors%22)">Anchors</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
                                href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Abrasives%22)">Abrasives</a>
                            <div class="dropdown-menu">
                                <ul class="level-1">
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Abrasives%22)AND(CategoryCode2:%22Brushes%22)">Brushes</a>
                                    </li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Abrasives%22)AND(CategoryCode2:%22Brush%20Accessories%22)">Brush
                                            Accessories</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
                                href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Saw%20Blades%22)">Saw
                                Blades</a>
                            <div class="dropdown-menu">
                                <ul class="level-1">
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Saw%20Blades%22)AND(CategoryCode2:%22Band%20Saw%20Blades%22)">Band
                                            Saw Blades</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Saw%20Blades%22)AND(CategoryCode2:%22Circular%20Saw%20Blades%22)">Circular
                                            Saw Blades</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Saw%20Blades%22)AND(CategoryCode2:%22Jig%20Saw%20Blades%22)">Jig
                                            Saw Blades</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
                                href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Hand%20Tools%22)">Hand
                                Tools</a>
                            <div class="dropdown-menu">
                                <ul class="level-1">
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Hand%20Tools%22)AND(CategoryCode2:%22Wrenches%22)">Wrenches</a>
                                    </li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Hand%20Tools%22)AND(CategoryCode2:%22Hammers,%20Striking,%20Demolition%20Tools%22)">Hammers,
                                            Striking, Demolition Tools</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown last"><a class="nav-link dropdown-toggle"
                                href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fleet%20Maintenance%22)">Fleet
                                Maintenance</a>
                            <div class="dropdown-menu">
                                <ul class="level-1">
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fleet%20Maintenance%22)AND(CategoryCode2:%22Automotive%20Fluids%22)">Automotive
                                            Fluids</a></li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fleet%20Maintenance%22)AND(CategoryCode2:%22Fluids%22)">Fluids</a>
                                    </li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fleet%20Maintenance%22)AND(CategoryCode2:%22Accessories%22)">Accessories</a>
                                    </li>
                                    <li class="level-1-item"><a class="level-1-link"
                                            href="http://ec.krisesystems.com/ECCatalog?SearchText=&amp;Page=1&amp;PerPage=10&amp;SortColumn=CategoryCode10&amp;Descending=Yes&amp;Filters=(CategoryCode1:%22Fleet%20Maintenance%22)AND(CategoryCode2:%22Cleaning%20Supplies%22)">Cleaning
                                            Supplies</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </nav>
    </header>
    `;


    document.getElementById("header-container").innerHTML = headerTemplate;
"use strict";

var mobileNav = document.getElementById('mobile-nav');

document.getElementById('mobile-nav-toggle').onclick = function () {
  mobileNav.classList.toggle('collapse');
};

document.getElementById('close-mobile-nav').onclick = function () {
  mobileNav.classList.toggle('collapse');
};